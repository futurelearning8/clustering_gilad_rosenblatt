import numpy as np
from abc import ABC, abstractmethod


class Model(ABC):
    """Interface for a model."""

    @abstractmethod
    def fit(self, X, y=None):
        """
        Fit model to feature set X and target y.

        :param np.ndarray X: feature set to fit model on.
        :param np.ndarray y: target to fit model on.
        """
        pass

    @abstractmethod
    def predict(self, X):
        """
        Predict on feature set X using the model.

        :param np.ndarray X: feature set to predict for.
        :return np.ndarray: predicted target for given feature set.
        """
        pass


class KMeans(Model):
    """
    Model that uses the K-means clustering algorithm to fit clusters to a set of points.
    Mimics the sklearn API: <https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html>.
    """

    def __init__(self, n_clusters=8, max_iter=300, tol=1e-4, random_state=None, progress_callback=None):
        """
        Initialize a k-means model with given hyperparameters.

        :param int n_clusters: the number of clusters to form as well as the number of centroids to generate.
        :param int max_iter: maximum number of iterations of the k-means algorithm.
        :param float tol: relative tolerance of difference in cluster centers to declare convergence.
        :param int random_state: determines random number generation for centroid initialization.
        :param collections.Callable progress_callback:
        """

        # Save hyperparameters.
        self.n_clusters = n_clusters
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state

        # Initialize placeholder for feature space dimensions.
        self._n_features = None

        # Initialize placeholder for cluster labels on training samples and number of iterations done at training.
        self.n_iter_ = None
        self.labels_ = None

        # Initialize placeholder for cluster centers array.
        self.cluster_centers_ = None

        # Initialize a plotter callback method to be called at each training iteration.
        self._progress_callback = progress_callback

    def fit(self, X, y=None):
        """
        Compute k-means clustering.

        :param np.ndarray X: training instances to cluster, given as an ndarray of shape (n_samples, n_features)..
        :param y: not used, present here for API consistency by convention.
        :return KMeans: trained model.
        """

        # Save sample-feature space dimension.
        n_samples, self._n_features = X.shape

        # Initialize seed for random state if provided.
        if self.random_state:
            np.random.seed(self.random_state)

        # Randomly initialize cluster centroids from data.
        replace = self.n_clusters > n_samples
        cluster_centers = X[np.random.choice(n_samples, self.n_clusters, replace=replace), :].copy()

        # Iteratively relocate cluster centers according to the k-means algorithm.
        n_iter = 0
        change = float("inf")
        labels = None
        while change > self.tol and n_iter < self.max_iter:

            # Calculate closest cluster for each sample: [sample_1_label, ..., sample_n_label].
            labels = np.argmin(np.linalg.norm(X - cluster_centers[:, np.newaxis], axis=2), axis=0)

            # Call plotter callback (it is the only stage in iteration that labels are consistent with cluster centers).
            if self._progress_callback:
                self._progress_callback(X, cluster_centers, labels)

            # Recalculate new cluster centers according to new cluster labels.
            new_cluster_centers = np.ones(shape=cluster_centers.shape)
            for this_cluster in range(self.n_clusters):

                # Split empty and non-empty labels-per-cluster cases (to avoid mean-ing an empty of all-NaN slice).
                samples_for_this_cluster = X[labels == this_cluster]
                if np.any(samples_for_this_cluster):
                    # Calculate new cluster center as the centroid of all samples associated with it.
                    new_cluster_centers[this_cluster] = np.mean(samples_for_this_cluster, axis=0)
                else:
                    # Broadcast NaNs for this cluster center.
                    new_cluster_centers[this_cluster] = np.nan

            # Copy new cluster centers to cluster_centers variable (slicing will cause side-effects at assignments).
            cluster_centers = new_cluster_centers.copy()

            # Calculate the max-norm of difference in cluster centers (ignore NaNs, no case of an all-NaN slice).
            change = np.nanmax(np.linalg.norm(cluster_centers - new_cluster_centers, axis=1))

            # Increment iteration number (for the next iteration).
            n_iter += 1

        # Save cluster centers, training labels and number of training iterations.
        self.n_iter_ = n_iter
        self.labels_ = labels
        self.cluster_centers_ = cluster_centers

        # Support chaining syntax.
        return self

    def predict(self, X):
        """
        Predict the closest cluster each sample in X belongs to.

        :param np.ndarray X: new data to predict on, given as an ndarray of shape (n_samples, n_features).
        :return np.ndarray: index of the cluster each sample belongs to in an ndarray of shape (n_samples,).
        """
        # Calculate closest cluster for each sample: [cluster_index_for_sample_1, ..., cluster_index_for_sample_n].

        # Verify consistency in number of features per sample.
        _, n_features = X.shape
        assert n_features == self._n_features

        # Predict labels from trained cluster centers.
        cluster_labels = np.argmin(np.linalg.norm(X - self.cluster_centers_[:, np.newaxis], axis=2), axis=0)

        # Return predicted labels.
        return cluster_labels

    def fit_predict(self, X, y=None):
        """
        Compute k-means clustering and predict the closest cluster each sample in X belongs to.

        :param np.ndarray X: training instances to train and predict for as ndarray of shape (n_samples, n_features).
        :param y: not used, present here for API consistency by convention.
        :return np.ndarray: index of the cluster each sample belongs to in an ndarray of shape (n_samples,).
        """

        # Fit clusters to training data.
        self.fit(X)

        # Return labels saved during training as predictions (instead of re-calculating).
        return self.labels_


def main():
    """Simple example for training and predicting with KMeans."""
    model = KMeans(n_clusters=5, random_state=42)
    X_train = np.random.random_sample(size=(200, 2))
    X_test = np.random.random_sample(size=(100, 2))
    print("labels: ", model.fit(X_train).predict(X_test))


if __name__ == "__main__":
    main()
