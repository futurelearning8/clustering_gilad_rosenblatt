import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans


def main():

    # Load and scale petri dish data and extract points as numpy arrays.
    X = StandardScaler().fit_transform(pd.read_csv("../data/bacteria.txt").values)
    df = pd.DataFrame(X, columns=["x", "y"])

    # Fit a k-means and DNSCAN clustering models to the data.
    model1 = KMeans(n_clusters=4, random_state=42).fit(X)
    model2 = DBSCAN(eps=0.3, min_samples=10).fit(X)

    # Plot the clusters side-by-side.
    fig, ax = plt.subplots(nrows=2, ncols=1)
    sns.scatterplot(x="x", y="y", data=df, hue=model1.labels_, ax=ax[0])
    sns.scatterplot(x="x", y="y", data=df, hue=model2.labels_, ax=ax[1])

    # Annotate.
    ax[0].set_title("KMeans")
    ax[1].set_title("DBscan")
    plt.tight_layout()
    plt.show(block=False)

    # Save figure
    plt.savefig("../images/perfect_clustering.png")


if __name__ == "__main__":
    main()
