import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.spatial.distance import cdist
from sklearn.cluster import KMeans
from models import KMeans as MyKMeans


class PetriDish:
    """A petri dish with colonies of god-awful bacteria."""

    def __init__(self):
        """Initialize a petri dish with bacteria coordinates read from file."""
        self._points = pd.read_csv("../data/bacteria.txt")

    @property
    def points(self):
        """
        :return pd.Series: the scatter of points as a pandas series of shape (num_points,) with index ["x", "y"].
        """
        return self._points

    def plot(self, **kwargs):
        """Visualize the disagreeable bunch of prokaryotic microorganisms that consider this petri dish home."""
        sns.scatterplot(x="x", y="y", data=self._points, **kwargs)
        plt.show(block=False)


def plot_clusters(n_clusters=4):
    """
    Compares cluster distribution between my KMeans implementation and sklearn's model for the petri dish dataset.

    :param int n_clusters: number of clusters to fit to data.
    """

    # Read petri dish data and extract points as numpy arrays.
    dish = PetriDish()
    X = dish.points.values

    # Fit two k-means models to the data: scikit-learn KMeans and my implementation.
    model1 = KMeans(n_clusters=n_clusters, random_state=42).fit(X)
    model2 = MyKMeans(n_clusters=n_clusters, random_state=42).fit(X)

    # Plot the clusters side-by-side.
    fig, ax = plt.subplots(nrows=2, ncols=1)
    dish.plot(hue=model1.labels_, ax=ax[0])
    dish.plot(hue=model2.labels_, ax=ax[1])

    # Annotate.
    ax[0].set_title("KMeans")
    ax[1].set_title("MyKMeans")
    plt.tight_layout()
    plt.show()


def plot_elbow():
    """Compares elbow curves for my KMeans implementation and sklearn's model for the petri dish dataset."""

    # Read petri dish data and extract points as numpy arrays.
    X = PetriDish().points.values

    # Initialize distortion lists and k values.
    distortions1 = []
    distortions2 = []
    k_values = range(1, 11)

    # Iterate over number of clusters and calculate distortions.
    for this_k in k_values:

        # Train models for this k value.
        model1 = KMeans(n_clusters=this_k).fit(X)
        model2 = MyKMeans(n_clusters=this_k).fit(X)

        # Calculate distortions based on Euclidean distance of data points from cluster centers.
        distortions1.append(sum(np.min(cdist(X, model1.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
        distortions2.append(sum(np.min(cdist(X, model2.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])

    # Plot elbow curves side-by-side.
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.plot(k_values, distortions1, 'go-', label="KMeans")
    ax.plot(k_values, distortions2, 'ro-', label="MyKMeans")
    ax.set_xlabel('Number of clusters')
    ax.set_ylabel('Distortion')
    ax.set_title('KMeans: elbow curve')
    ax.legend()
    plt.show()


if __name__ == "__main__":
    plot_clusters(n_clusters=4)
